package com.api.dock.model.enums;

public enum StatusEnum {
    APPROVED, DISAPPROVED, REVIEW
}
